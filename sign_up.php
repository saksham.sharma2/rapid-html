<?php include('common/header2.php') ?>

<!-- ==== Sign Up Start ==== -->
<section class="sign_up_section">
    <div class="sign_up_area top-space">
        <div class="left_area">
            <div class="left_inne">
                <div class="info_area">
                    <h1>Sign Up</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit
                        rutrum.</p>
                </div>
                <div class="back-img d-md-block d-none">
                    <img src="images/Rectangle20.png" alt="...." />
                </div>
                <div class="bottom-img d-md-block d-none">
                    <img src="images/Layers1.png" alt="">
                </div>
            </div>
        </div>
        <div class="right_area">
            <div class="steps_formed">
                    <div class="query-form">
                        <form>
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form_steps1">
                                        <div class="top_bar">
                                            <div class="line_bar">
                                                <div class="bar_inn" style=width:0%></div>
                                                <div class="bar_wrap">
                                                <div class="bar_circle circle_1"></div>
                                                <div class="bar_circle circle_2"></div>
                                                <div class="bar_circle circle_3"></div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="set_head">
                                            <h1>Enter your detail</h1>
                                        </div>
                                        <div class="row">
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>First name</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter your first name">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Last name</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter your last name">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control" name=""
                                                        placeholder="Enter your email address">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Username</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter username">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="exist">
                                                    <h6>Do you have an existing number to use? </h6>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefaulte"
                                                            id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            Yes
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefaulte"
                                                            id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter phone number">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="exist">
                                                    <h6>Do you have a smart phone or device to use?  </h6>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefaults"
                                                            id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            Yes
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefaults"
                                                            id="flexRadioDefault4">
                                                        <label class="form-check-label" for="flexRadioDefault4">
                                                            no
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>IMEI number</label>
                                                    <p>Dial *#06# on your phone to access IMEI</p>
                                                    <input type="text" class="form-control" name=""
                                                        placeholder="Enter your device IMEI number" />
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="view">
                                                    <a href="javascript:;" class="btn btn-primary-1 next_steps_1">Continue</a>
                                                </div>
                                            </div>
                                        </div>                                                                                     
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form_steps2 d-none">
                                        <div class="top_bar">
                                            <div class="line_bar">
                                                <div class="bar_inn" style=width:50%></div>
                                                <div class="bar_wrap" >
                                                    <div class="bar_circle circle_1"></div>
                                                    <div class="bar_circle circle_2"></div>
                                                    <div class="bar_circle circle_3"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="set_head">
                                            <h1>Enter your address</h1>
                                            <h4>Service address</h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Street</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter street">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Select state</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>ZIP code</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter ZIP code">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h4>Billing address</h4>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Street</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter street">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter city">
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Select state</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>ZIP code</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter ZIP code">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="view">
                                                    <a href="javascript:;" class="btn btn-primary-1 next_steps_2">Continue</a>
                                                </div>       
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form_steps3 d-none">
                                        <div class="top_bar">
                                            <div class="line_bar">
                                                <div class="bar_inn" style=width:100%></div>
                                                <div class="bar_wrap">
                                                    <div class="bar_circle circle_1"></div>
                                                    <div class="bar_circle circle_2"></div>
                                                    <div class="bar_circle circle_3"></div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="set_head">
                                            <h1>Set your password</h1>
                                        </div>
                                        <div class="row">
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" class="form-control" name="" placeholder="Enter your password">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label>Confirm password</label>
                                                    <input type="text" class="form-control" name="" placeholder="Confirm your password">
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="view">
                                                    <a href="dashboard_home.php" class="btn btn-primary-1">Let's Go</a>
                                                </div>                                                                                      
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== Header === -->
<?php include('common/footer.php') ?>