<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
        <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <div class="heading_area">
                <a href="dashboard_home.php"><h1> <i class="fal fa-long-arrow-left pe-2"></i> Home</h1></a>
            </div>
            <div class="usage_main">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="columns_areas">
                                <div class="view_detail_area_modify">
                                    <div class="row">
                                        <div class="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="left_area">
                                                <div class="inner_info">
                                                    <div class="contact_info">
                                                        <a href="#">
                                                            <div class="mobile_info"> +1 (408) 746-6060</div>
                                                        </a>
                                                        <p> Dianne Jane</p>
                                                    </div>
                                                    <div class="service_address">
                                                        <h3>Service Address</h3>
                                                        <p>Dianne Jane</p>
                                                        <a href="#">
                                                            <div class="location_info">1820 Bennett Point Rd.
                                                                Queenstown, MD 21658</div>
                                                        </a>
                                                    </div>
                                                    <div class="device_info">
                                                        <h3>Device Info</h3>
                                                        <p>Activation Date : March 26, 2022</p>
                                                        <p>IMEI : 355070814769240</p>
                                                        <p>SIM Card Number : 89014103273626984250</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-5 col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="right_area">
                                                <div class="icon_box">
                                                    <img src="images/globe.png" alt="">
                                                </div>
                                                <div class="item text-center">
                                                    <h1>Modify plan</h1>
                                                    <p>Lorem ipsum dolor sit amet, elit. Pellentesque ex elit.</p>
                                                    <div class="view">
                                                        <a href="plan.php" class="btn btn-primary-1 ">Modify plan</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ps-sm-0">
                            <div class="usages">
                                <div class="usage_wrap">
                                    <div class="head">
                                        <h4>
                                            Usage
                                        </h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                            <div class="first_wrap">
                                                <canvas id="donutChart" width="100%" height="50"></canvas>
                                                <div class="limites">
                                                    <p>
                                                        Unlimited
                                                    </p>
                                                    <a href="#" class="btn btn-primary-1">View usage</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                            <div class="first_wrap">
                                                <canvas id="donutChart1" width="100%" height="50"></canvas>
                                                <div class="limites">
                                                    <p>
                                                        Unlimited
                                                    </p>
                                                    <a href="#" class="btn btn-primary-1">View usage</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                            <div class="first_wrap">
                                                <canvas id="donutChart2" width="100%" height="50"></canvas>
                                                <div class="limites">
                                                    <p>
                                                    5.0GB of 5.0GB remaining
                                                    </p>
                                                    <a href="#" class="btn btn-primary-1">View usage</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>