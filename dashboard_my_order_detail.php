<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <!-- <div class="heading_area">
                <h2>My order</h2>
            </div> -->
            <div class="my_order_detail">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-xxl-8 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="left_area">
                                    <div class="left_header">
                                        <a href="dashboard_my_order.php">
                                            <h2><i class="far fa-long-arrow-left"></i>My order</h2>
                                        </a>
                                    </div>
                                    <div class="inner_area">
                                        <p>Order id #5687 <a href="javascript:;">Activated</a></p>
                                        <p>Phone number : +1 456 2569 4895</p>
                                        <p>Number of lines : 1 line</p>
                                        <p>Payment method : paypal.me/Diannejane</p>
                                        <p>Payment status : paid</p>
                                        <p>Address : 12 lorem, new donat town, california</p>
                                        <p>Activate on 12feb,2022</p>
                                        <p>Expire on 12march,2022</p>
                                    </div>
                                    <div class="inform_area">
                                        <div class="left_content">
                                            <h5>5GB</h5>
                                        </div>
                                        <div class="right_content">
                                            <p>Expires on 11 Apr, 2022 10:22 PM</p>
                                        </div>
                                        <div class="content">
                                            <p>Unlimited text and talk</p>
                                            <p><span>$49.00</span> /per month</p>
                                            <p>1 lines</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="right_area">
                                    <h2>Payment detail</h2>
                                    <div class="inner_area">
                                        <div class="price_area">
                                            <ul>
                                                <li>
                                                    <div class="left">
                                                        <p>
                                                            Price
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <p>
                                                            $24.00
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="left">
                                                        <p>
                                                            Activation fee
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <p>
                                                            $25.00
                                                        </p>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="total_area">
                                            <div class="left">
                                                <p>
                                                    Total
                                                </p>
                                            </div>
                                            <div class="right">
                                                <p>
                                                    $49.00
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>