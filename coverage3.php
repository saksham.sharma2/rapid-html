<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Reset Password Section Start === -->
<section class="coverage_section_range_second top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="header_content">
                    <h1>Coverage</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. .
                    </p>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="header_img">
                    <img src="images/coverage_map.png" alt="image not found" />
                    <div class="btn_area">
                        <div class="view">
                            <a href="map.php" class="btn btn-primary-1">Map View</a>
                        </div>
                    </div>
                </div>
             </div>
            <div class="col-xxl-5 col-xl-5 col-lg-7 col-md-6 col-sm-10 col-12 mx-auto">
                <div class="coverage_section_area text-center">
                    <div class="box_area">
                        <h5>Coverage</h5>
                        <div class="image_coverage_area">
                            <div class="coverage_range">
                                <div class="first_range  excellent">  
                                <!-- excellent  good poor -->
                                    <div class="second_range">
                                        <div class="tower">
                                            <img src="images/Radio-Towesr.png" alt=".." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5>Excellent</h5>
                          </div>
                       
                        <h5>Device</h5>
                        <div class="image_area">
                            <img src="images/smartphone_approve.png" alt="..." />
                        </div>
                        <h6>Not compatible</h6>
                        <div class="info_area">
                            <h6>Apple iphone X</h6>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis
                                suscipit rutrum. </p>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Reset Password Section End === -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>