<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <div class="heading_area el">
                <h2>Change password</h2>
            </div>
            <div class="change_password">
                <form>
                    <div class="container">
                        <div class="row">
                            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-10 col-sm-12 col-12 mx-auto">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="old_password">Old password</label>
                                            <input type="text" class="form-control" placeholder="Enter your password" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="new_password">New password</label>
                                            <input type="text" class="form-control" placeholder="Enter new password" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="confirm_password">Confirm password</label>
                                            <input type="text" class="form-control" placeholder="Confirm new password" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="button">
                                            <a href="javascript:;" class="btn btn-primary-1" >Change password</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</section>







<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>