<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <div class="heading_area el">
                <h2>Settings</h2>
            </div>
            <div class="setting">
                <form>
                    <div class="container">
                        <div class="row">
                            <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-10 col-sm-12 col-12 mx-auto">
                                <div class="header_area">
                                    <div class="left_area">
                                        <p>Edit profile<p>
                                    </div>
                                    <div class="right_area">
                                        <a href=javascript:; class="btn btn-primary-1">Save changes</a>
                                    </div>
                                </div>
                                <div class="photo">
                                    <p>Photo</p>
                                </div>
                                <div class="image">
                                    <img src="images/Rectangle 192.png" alt="image not found" />
                                    <div class="iconwrap">
                                        <a href="#">
                                        <i class="fal fa-pen"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="first_name">First name</label>
                                            <input type="text" class="form-control" placeholder="Enter first name" value="Dianne" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="last_name">Last name</label>
                                            <input type="text" class="form-control" placeholder="Enter last name" value="Jane" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" placeholder="Enter email" value="diannejane@samplemail.com" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="user_name">Username</label>
                                            <input type="text" class="form-control" placeholder="Enter username" value="Dianne.1998" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="phone_number">Phone number</label>
                                            <input type="text" class="form-control" placeholder="Enter phone number" value="+1 456 2568 3659" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="imei_number">IMEI number</label>
                                            <p>Dial *#06# on your phone to access IMEI</p>
                                            <input type="text" class="form-control" placeholder="Enter phone IMEI number" value="123456789123456" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h2>Service address</h2>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" placeholder="Enter street" value="16 st lorem ipsum" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" placeholder="Enter city" value="los angeles" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-select">
                                                <option>Select state</option>
                                                <option value="California" selected>California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="zip_code">ZIP code</label>
                                            <input type="text" class="form-control" placeholder="Enter ZIP code" value="90009" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h2>Billing address</h2>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" placeholder="Enter street" value="16 st lorem ipsum" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" placeholder="Enter city" value="los angeles" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-select">
                                                <option>Select state</option>
                                                <option value="California" selected>California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="zipcode">ZIP code</label>
                                            <input type="text" class="form-control" placeholder="Enter ZIP code" value="90009" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</section>







<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>