<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Reset Password Section Start === -->
<section class="coverage_section top-space">
<div class="container">
    <div class="row">
        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
            <div class="header_content">
                <h3>Check coverage</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
            </div>
        </div>
        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="header_img">
                <img src="images/coverage_map.png" alt="image not found" />
            </div>
        </div>
        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-7 col-sm-12 col-12 mx-auto">
            <div class="coverage_section_area">
                <form>
                    <div class="form_step1">
                        <div class="box_area">
                            <div class="top_bar">
                                <div class="line_bar"></div>
                                <div class="bar_circle circle_1"></div>
                                <div class="bar_circle circle_2"></div>
                            </div>
                            <h2>Enter your address</h2>
                            <h5>Check the coverage in your area.</h5>
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="street">Street</label>
                                        <input type="text" class="form-control" placeholder="Enter street" />
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control" placeholder="Enter city" />
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <select class="form-select">
                                            <option>Enter state</option>
                                            <option value="">One</option>
                                            <option value="">Two</option>
                                            <option value="">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="zip_code">ZIP code</label>
                                        <input type="text" class="form-control" placeholder="Enter ZIP code" />
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="button">
                                        <a href="javascript:;" class="next_step_2 btn btn-primary-1">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="form_step2">
                        <div class="box_area">
                            <div class="top_bar">
                                <div class="line_bar"></div>
                                <div class="bar_circle circle_1"></div>
                                <div class="bar_circle circle_2"></div>
                            </div>
                            <h2>Select device</h2>
                            <h5>Select your device brand and model</h5>
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="brand">Brand</label>
                                        <select class="form-select">
                                            <option>Select your device brand</option>
                                            <option value="">One</option>
                                            <option value="">Two</option>
                                            <option value="">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="model">Model</label>
                                        <select class="form-select">
                                            <option>Select your device model</option>
                                            <option value="">One</option>
                                            <option value="">Two</option>
                                            <option value="">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="select_device_option_head">
                                        <p>or</p>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="imei_number">IMEI number</label>
                                        <h6>Dial *#06# on your phone to access IMEI</h6>
                                        <input type="text" class="form-control" placeholder="Enter your device IMEI number" />
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="button">
                                        <a href="coverage3.php" class="next_step_3 btn btn-primary-1">Let's go</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<!-- ==== Reset Password Section End === -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>

