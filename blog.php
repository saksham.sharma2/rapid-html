<?php include('common/header.php') ?>


<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>Blogs</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->

.Layer_0 {
  background-image: url("Layer 0.png");
  position: absolute;
  left: 0px;
  top: 0px;
  width: 357px;
  height: 188px;
  z-index: 1;
}


<!-- ==== Blog Section Start ==== -->
<section class="blog-wrap frome-blog">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog-inner">
                <div class="search_tags">
                        <div class="next_prev prev_btn" id="left-button"><i class="far fa-angle-left"></i></div>
                            <div class="filter_options_row">
                                <ul class="list-inline filter-options">
                                    <li class="btn btn-primary active"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                    <li class="btn btn-primary"><a href="javascript:;">lorem</a></li>
                                </ul>
                            </div>
                        <div class="next_prev next_btn" id="right-button"><i class="far fa-angle-right"></i></div>
                    </div>
                    <div class="blog_wrap blog_columns">
                     <div class="row" id="grid">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_1"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_1.png" alt="..." />
                                      </a>                                    
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item " data-groups='["category_2"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">  
                                            <img src="images/blog_2.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content ">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_3"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_3.png" alt=".." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_4"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_4.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_2"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_5.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_1"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_6.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_4"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_7.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item" data-groups='["category_4"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                         <img src="images/blog_8.png" alt="..." />
                                        </a>                                        
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col- picture-item " data-groups='["category_2"]'>
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_9.png" alt="....." />
                                        </a>    
                                    </div>
                                    <div class="content">
                                        <div class="match_height_txt">
                                            <div class="date q">
                                                <p> November 18, 2021</p>
                                            </div>
                                            <p>
                                                <a href="blog_detail.php">
                                                Lorem ipsum dolor sit amet,
                                                </a>
                                            </p>
                                        </div>
                                        <div class="match_height_col">
                                            <span>
                                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                            </span>
                                        </div>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 my-sizer-element"></div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div class="view float-end mt-sm-5 mt-2">
                                <a href="javascript:;" class="btn btn-primary-1">+ View more</a>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
        </div>
    </div>
</section>
<!-- ==== Blog Section end ==== -->

<!-- ==== Header === -->
<?php include('common/footer.php') ?>