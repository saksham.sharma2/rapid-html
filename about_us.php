<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>About us</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->

<!-- ==== About Section start ==== -->
<section class="about_sections">
<div class="container">
    <div class="row">
        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row align-items-center">
                <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12">
                    <div class="lefted">
                        <div class="image">
                            <img src="images/aboutret.png" alt="..." />
                            <div class="usr">
                                <img src="images/1.jpg" alt="..." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-8 col-xl-8 col-lg-7 col-md-6 col-sm-6 col-12">
                    <div class="content">
                        <h4>Who we are</h4>
                        <p>Rapid Datacom is the US Veteran’s wireless provider. Our service is based on dependable, 
                            nationwide 4G and 5G LTE networks. We are committed to providing veterans with this 
                            dependable wireless service and exceptional support, while supporting other veterans 
                            through contributions to organizations such as Platoon 22 and Heroic Reins.
                        </p>
                        <p>When you partner with us, you will know that you too are contributing to the recovery of our military Veterans!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="about_sections el">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row align-items-center">
                    <div class="col-xxl-8 col-xl-8 col-lg-7 col-md-6 col-sm-6 col-12 order-sm-0 order-1">
                        <div class="content left">
                            <h4>Our mission</h4>
                            <p>Our mission is to provide great mobile service that supports causes dedicated to the health and recovery of US Veterans.</p>
                        </div>
                    </div>
                    <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12">
                        <div class="righted">
                            <div class="image">
                                <div class="usr">
                                    <img src="images/1.jpg" alt="..." />
                                </div>
                                <img src="images/Rectangle-19.png" alt="..." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="about_sections">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row align-items-center">
                        <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12">
                            <div class="lefted">
                                <div class="image">
                                    <img src="images/aboutret.png" alt="..." />
                                    <div class="usr">
                                        <img src="images/1.jpg" alt="..." />
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 col-sm-6 col-12">
                            <div class="content">
                                <h4>Our vision</h4>
                                <h3>Improving the lives of <span>US Veterans.</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="about_sections el">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">    
                <div class="row align-items-center">
                    <div class="col-xxl-8 col-xl-8 col-lg-7 col-md-6 col-sm-6 col-12 order-sm-0 order-1">
                        <div class="content left">
                            <h4>Values</h4>
                            <p>We support organizations with shared values. Our focus is to ensure US Veterans
                                recovery programs are provided the resources necessary. 
                            </p>
                        </div>
                    </div>        
                    <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12">
                        <div class="righted">
                            <div class="image">
                                <div class="usr">
                                    <img src="images/1.jpg" alt="..." />
                                </div>
                                <img src="images/Rectangle-19.png" alt="..." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_sections ekt">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"> 
                <div class="row align-items-center">
                    <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12">
                        <div class="foot_image el">
                            <img src="images/Vector_1.png" alt="..." />
                        </div>
                    </div>        
                    <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 col-sm-6 col-12">
                        <div class="content">
                            <h2>Join Our network</h2>
                            <h4>Are you ready to join us ?</h4>
                            <p>Lorem ipsum sit dor el ipsum dor dolor sit ipsum lorem dor  dolor sit sat lorem ipsum dor sit. 
                            </p>
                            <div class="button">
                                <a href="#javascript" class="btn btn-primary-1">Join</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== About Section End ==== -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>