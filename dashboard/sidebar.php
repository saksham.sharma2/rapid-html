<div class="side_bar_section">
    <div class="side_bar_area sticky-top">
        <div class="box_area">
            <div class="image_area">
                <a href="javascript:;">
                    <div class="user-img-warp">
                        <div class="user_img">
                            <img src="images/profile_pic.png" alt="..." />  
                        </div>
                   </div>
                </a>
                <div class="name">Dianne Jane</div>
          </div>
            <div class="menu_area">
                <ul class="d-none d-md-block">
                    <li class="active">
                        <a href="dashboard_home.php">
                            <div class="icon"><i class="fal fa-home-alt"></i></div>
                            <div class="text">Home</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_my_order.php">
                            <div class="icon"><i class="fi fi-rr-shopping-cart"></i></div>
                            <div class="text">My orders</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_transaction.php">
                            <div class="icon"><i class="far fa-file-alt"></i></div>
                            <div class="text">My transactions</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_setting.php">
                            <div class="icon"><i class="far fa-cog"></i></div>
                            <div class="text">Settings</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_change_password.php">
                            <div class="icon"><i class="far fa-lock"></i></div>
                            <div class="text">Change password</div>
                        </a>
                    </li>
                     <li>
                        <a href="index.php">
                            <div class="icon"><i class="far fa-sign-out"></i></div>
                            <div class="text">Logout</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
