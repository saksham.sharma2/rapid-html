<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Reset Password Section Start === -->
<section class="login_section top-space">
<div class="container">
    <div class="row">
        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-7 col-sm-12 col-12 mx-auto">
            <div class="login_section_area">
                <div class="header_area">
                    <h3>Login</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                </div>
                <div class="box_area">
                    <h2>Welcome back!</h2>
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Username</label>
                                    <input type="text" class="form-control" placeholder="Enter username or email" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" placeholder="Enter password" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="forgot_button">
                                    <a href="forgot_password.php">Forgot password?</a>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="login_button">
                                    <a href="dashboard_home.php" class="btn btn-primary-1">Login</a>
                                </div>
                                <div class="sign_up_button">
                                    <p>Don't have an account? <a href="sign_up.php">Sign Up</a></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- ==== Reset Password Section End === -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>

