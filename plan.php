<?php include('common/header.php') ?>

<!-- ==== Plan Section Start ==== -->
<section class="membership_plan_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="membership_plan_area top-space">
                    <div class="detail_area text-center">
                        <h1>Select Plan</h1>
                        <p>With Rapid Datacom, there are no contracts. If you need more or
                            less data, simply change to a plan that meets your needs.</p>
                        <h2>How many lines will you have on your plan?</h2>
                        <div class="off_area">
                            <p> Save 10% on your plan on two or more lines!</p>
                        </div>
                        <div class="choose_btn text-center">
                            <ul class="btn_area">
                                <li class="active">
                                    <a href="javascript:;" class="btn btn-primary-6">1 line</a>
                                </li>
                                <li class="search_li">
                                    <a href="javascript:;" class="btn btn-primary-6">2 line</a>
                                </li>
                                <li class="search_li">
                                    <a href="javascript:;" class="btn btn-primary-6">3 line</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                  <div class="plan_detail">
                    <div class="row">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            1GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $24
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                            <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            3GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $34
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                        <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            5GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $44
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                        <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            12GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $54
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                        <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            12GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $54
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                        <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="items_detail_area">
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="plan_gb">
                                            12GB
                                        </div>
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="pice">
                                            <h2>
                                                $54
                                            </h2>
                                            <p>
                                                Per line
                                            </p>
                                        </div>
                                        <div class="benfit">
                                            <ul>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit amet.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit.
                                                    </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Lorem ipsum dolor sit Amet, dor dolor.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="view">
                                        <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Plan Section end ==== -->


<!-- ==== Header === -->
<?php include('common/footer.php') ?>