<?php include('common/header.php') ?>


<section class="order_success_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-5 col-xl-5 col-lg-6 col-md-8 col-sm-10 col-12 mx-auto">
                <div class="detail_area text-center">
                    <h1>Order placed successfully.</h1>
                    <div class="inner_area">
                        <div class="box_area">
                            <div class="image_area">
                                <img src="images/shopping-bag.png" alt="..." />
                            </div>
                            <div class="contact_info">
                                <p>Number of line : 1 line</p>
                                <p>Phone number : +1 456 3256 1564</p>
                                <p>IMEI : 355070814769240</p>
                            </div>
                            <div class="view">
                                <a href="#" class="btn btn-primary-1">My order</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


































<!-- ==== Header === -->
<?php include('common/footer.php') ?>