<?php include('common/header.php') ?>


<!-- ==== Breadcame Section Start ==== -->
<!-- <section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>Blog Detail</h2>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- ==== Breadcame Section End ==== -->


<!-- ==== Blog Section Start ==== -->
<section class="cms_pages_section blog_detail_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_detail_area">
                    <div class="row">
                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <div class="blog_detail_content">
                            <a href="blog.php"><i class="fal fa-long-arrow-left"></i>Blogs</a>
                                   <h1>
                                   Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit. Nullam maximus 
                                    </h1>
                                    <div class="date">Sat, November 05 </div>
                                <div class="img_area">
                                    <img src="images/blog_detail.png" alt="..." />
                                </div>
                            
                                <div class="content_box">
                                   
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                                    </p>
                                     <p>
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                        Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    </p>
                                     <p>
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    </p>
                                    <p>
                                        Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    </p>
                                </div>
                                <div class="share">
                                    <ul>
                                        <li>
                                            Share:
                                        </li>
                                        <li>
                                            <a href="javascript:;"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="javascript:;"><i class="fab fa-instagram"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div class="blog_sidebar">
                                <div class="tags_box">
                                    <h1>New Posts</h1>
                                    <div class="new_posts">
                                    <ul>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                        <li>
                                        <p>
                                            <a href="#">
                                                Lorem ipsum dolor  sit amet, con sectetur elit adipiscing elit con sectetur elit. 
                                            </a>
                                        </p>
                                        </li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Blog Section End ==== -->

<!-- ==== blog === -->
<section class="frome-blog blog-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                 <div class="heading-lines">
                     <h1>
                     Related Posts
                     </h1>
                 </div>
                 <div class="blog_wrap">
                     <div class="row">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_7.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="date q">
                                            <p> November 18, 2021</p>
                                        </div>
                                        <p>
                                            <a href="blog_detail.php">
                                            Lorem ipsum dolor sit amet,
                                            </a>
                                        </p>
                                        <span>
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                        </span>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_8.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="date q">
                                            <p> November 18, 2021</p>
                                        </div>
                                        <p>
                                            <a href="blog_detail.php">
                                            Lorem ipsum dolor sit amet,
                                            </a>
                                        </p>
                                        <span>
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                        </span>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 d-lg-block d-none">
                            <div class="blog_one_forth">
                                <div class="blog-inner">
                                    <div class="blog-inner-img">
                                        <a href="blog_detail.php">
                                            <img src="images/blog_9.png" alt=".." />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="date q">
                                            <p> November 18, 2021</p>
                                        </div>
                                        <p>
                                            <a href="blog_detail.php">
                                            Lorem ipsum dolor sit amet,
                                            </a>
                                        </p>
                                        <span>
                                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam
                                        </span>
                                        <a href="blog_detail.php" class="read-more">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div>
</section>



<!-- ==== Header === -->
<?php include('common/footer.php') ?>