<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
        <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <div class="heading_area">
                <h2>Hello Dianne</h2>
                <p>Welcome back!</p>
            </div>
            <div class="columns_areas">
                <div class="top_boxes_area">
                    <div class="row">
                    <div class="col-xxl-6 col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="inner_box_area match_height_col">
                                <div class="plan_gb">5GB</div>
                                <div class="item">
                                    <div class="plans_ment">
                                        <div class="desp">
                                            <p>
                                                Unlimited text and talk
                                            </p>
                                        </div>
                                        <div class="price">
                                            <h2>
                                                $44.00<sub>/per month</sub>
                                            </h2>
                                        </div>
                                        <div class="line_area">
                                            <p>2 lines</p>
                                        </div>
                                        <div class="expire_area">
                                            <p>Expires on 11 Apr, 2022 10:22 PM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="gradient_box_area match_height_col">
                                <div class="item">
                                    <div class="top_area">
                                        <div class="img_area">
                                            <img src="images/home_phone.png" alt="" />
                                        </div>
                                        <div class="view_border">
                                            <a href="#" class="btn btn-primary-7"  data-bs-toggle="modal" data-bs-target="#change_detail">Change</a>
                                        </div>
                                    </div>
                                    <div class="card_number">
                                        <h2>
                                            2564 2356 2654 8961
                                        </h2>
                                    </div>
                                   
                                        <div class="name_area">
                                            <p>Dianne Jane</p>
                                        </div>
                                        <div class="expire_area">
                                            <p>Expires on 11,2022</p>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="line_boxes_row text-center">
                    <div class="row">
                    <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="inner_box_area match_height_col">
                                <div class="plan_gb">Line 1</div>
                                <div class="item">
                                    <div class="activate_area">
                                        <p>Activate</p>
                                    </div>
                                    <div class="mobile_sim_info">
                                        <h6>+1 (408) 746-6060</h6>
                                        <p>Dianne Jane</p>
                                    </div>
                                    <div class="contact_info">
                                        <p>Apple iphone 12 pro</p>
                                        <p>994858460399701</p>
                                    </div>
                                    <div class="view">
                                        <a href="dashboard_home_view_detail.php" class="btn btn-primary-1 w-100">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="inner_box_area match_height_col">
                                <div class="plan_gb">Line 1</div>
                                <div class="item">
                                    <div class="activate_area">
                                        <p>Activate</p>
                                    </div>
                                    <div class="mobile_sim_info">
                                        <h6>+1 (408) 746-6060</h6>
                                        <p>Dianne Jane</p>
                                    </div>
                                    <div class="contact_info">
                                        <p>Apple iphone 12 pro</p>
                                        <p>994858460399701</p>
                                    </div>
                                    <div class="view">
                                        <a href="dashboard_home_view_detail.php" class="btn btn-primary-1 w-100">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="inner_box_area match_height_col">
                                <div class="item">
                                    <div class="add_icon_area">
                                        <div class="icon_plus_area">
                                            <a href="#" data-bs-toggle="modal" data-bs-target="#dashboard_inform_2">
                                                <i class="far fa-plus"></i>
                                            </a>
                                            <p>Add Line</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>