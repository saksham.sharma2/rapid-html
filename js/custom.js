$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 20) {
        $(".header-main-section").addClass("shadow");
    }
    else
    {
        $(".header-main-section").removeClass("shadow");
    }
}); //missing );


// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.bar').addClass('hide_bar');
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_menu');
    $('body').addClass('scroll_off');
});
}

// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_open');
    $('body').addClass('scroll_off');
});
}
/* Close menu */
if($('.cross_menu a').length)
{
$('body').on('click', '.cross_menu a', function(){
    $(this).parents('.responsive_menu').find('.open_menu.show_open').removeClass('show_open');
    $('body').removeClass('scroll_off');
});
}

// checkout page payment
if($('.paypal').length)
{
$('body').on('click', '.paypal', function(){
    $(this).parents('.accordion-body').find('.credit_detail').addClass('d-none');
    $(this).parents('.accordion-body').find('.paypals').removeClass('d-none');
});
}

// checkout page payment
if($('.credit').length)
{
$('body').on('click', '.credit', function(){
    $(this).parents('.accordion-body').find('.credit_detail').removeClass('d-none');
    $(this).parents('.accordion-body').find('.paypals').addClass('d-none');
});
}

// plans
if($('#plans').length)
{
$('#plans').owlCarousel({
    items:4,
    stagePadding: 10,
    loop:true,
    autoplay:true,
    autoplayTimeout:5000,
    margin:20,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        768:{
            items:2,
            margin:20,
            nav:false,
        },
        1070:{
            items:2,
            margin:20,
            nav:false,
        },
        1200:{
            items:4
        }
        
    }
})
}

// testimonial
if($('#testimonial').length)
{
$('#testimonial').owlCarousel({
    center: true,
    loop:true,
    margin:50,
    stagePadding: false,
    autoplay:true,
    autoplayTimeout:5000,
    nav:true,
    navText: ["<i class='fal fa-arrow-left'></i>","<i class='fal fa-arrow-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
        },
        600:{
            items:2,
            nav:false,
        },
        1070:{
            items:3,
            nav:false,
        },
        1200:{
            items:3
        }
    }
})
}

// Height match columns
$(document).ready(function () {
    $('.match_height_col').matchHeight();
    $('.match_height_txt').matchHeight();
});

// Scrolls to the selected menu item on the page

// coverage steps
if($('.next_step_2').length)
{
$('body').on('click', '.next_step_2', function(){
    that = $(this);
    parent = that.parents('.coverage_section_area');
    parent.find('.form_step1').addClass('d-none');
    parent.find('.form_step2').addClass('d-block');
});
}


// SIGNUP steps
if($('.next_steps_1').length)
{
$('body').on('click', '.next_steps_1', function(){
    that = $(this);
    parent = that.parents('.sign_up_area');
    parent.find('.form_steps1').addClass('d-none');
    parent.find('.form_steps2').removeClass('d-none');
});
}


if($('.next_steps_2').length)
{
$('body').on('click', '.next_steps_2', function(){
    that = $(this);
    parent = that.parents('.sign_up_area');
    parent.find('.form_steps2').addClass('d-none');
    parent.find('.form_steps3').removeClass('d-none');

});
}


if($('.next_step_2').length)
{
$('body').on('click', '.next_step_2', function(){
    that = $(this);
    parent = that.parents('.self_activation_section_area');
    parent.find('.form_step1').addClass('d-none');
    parent.find('.form_step2').addClass('d-block');
    parent.find('.self_active_wrap').addClass('d-block');
});
}

if($('.next_step_2').length)
{
$('body').on('click', '.next_step_2', function(){
    that = $(this);
    parent = that.parents('.self_active_wrap');
    parent.find('.footer_content').addClass('d-none');
});
};

// Start Date
if($('#start_date').length)
{
$('#start_date').datepicker({
    autoclose: false,
    container: '#start_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});
};

// new Date
if($('#new_date').length)
{
$('#new_date').datepicker({
    autoclose: false,
    container: '#new_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});
};

// End Date
if($('#end_date').length)
{

// End Date
$('#end_date').datepicker({
    autoclose: false,
    container: '#end_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});
};



// Slide tags from right button on car listing top tags
$('#right-button').click(function() {
    event.preventDefault();
    $('.search_tags .list-inline').animate({
        scrollLeft: "+=100px"
    }, "slow");
});

// Slide tags from left button on car listing top tags
$('#left-button').click(function() {
    event.preventDefault();
    $('.search_tags .list-inline').animate({
        scrollLeft: "-=100px"
    }, "slow");
});




  