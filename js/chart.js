const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
const legendMargin = {
    id: 'legendMargin',
    beforeInit(chart, legend, option){
        //console.log(chart.legend.fit);
        const fitValue = chart.legend.fit;
        chart.legend.fit = function fit(){
            fitValue.bind(chart.legend)();
            return this.height += 70;
        }
    }
}

// Donut Chart
const ctxDonut = document.getElementById('donutChart').getContext('2d');
const donutChart = new Chart(ctxDonut, {
    type: 'doughnut',
    data: {
        labels: [
            'Red'
        ],
        datasets: [
            {
                label: 'My First Dataset',
                data: [300],
                backgroundColor: [
                    'rgba(210, 5, 10, 1)',
                  ],
                hoverOffset: 1
            },
        ],
        labels: ['Red'],
    },
    options: {
        scales: {
            display: false
        },
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: false
            },
            datalabels: {
                formatter: (value) => {
                  return value + '%';
                }
            },
           
        },
        
    }
   
});

// Donut Chart
const ctxDonut1 = document.getElementById('donutChart1').getContext('2d');
const donutChart1 = new Chart(ctxDonut1, {
    type: 'doughnut',
    data: {
        labels: [
            'Red'
        ],
        datasets: [
            {
                label: 'My First Dataset',
                data: [300],
                backgroundColor: [
                    'rgba(210, 5, 10, 1)',
                  ],
                hoverOffset: 1
            },
        ],
        labels: ['Red'],
    },
    options: {
        scales: {
            display: false
        },
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: false
            },
            datalabels: {
                formatter: (value) => {
                  return value + '%';
                }
            }
        }
    }
});

// Donut Chart
const ctxDonut2 = document.getElementById('donutChart2').getContext('2d');
const donutChart2 = new Chart(ctxDonut2, {
    type: 'doughnut',
    data: {
        labels: [
            'Red'
        ],
        datasets: [
            {
                label: 'My First Dataset',
                data: [300, 120],
                backgroundColor: [
                    'rgba(210, 5, 10, 1)',
                    'rgba(210, 5, 10, 0.6)',
                  ],
                hoverOffset: 1
            },
        ],
        labels: ['Red'],
    },
    options: {
        scales: {
            display: false
        },
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: false
            },
            datalabels: {
                formatter: (value) => {
                  return value + '%';
                }
            }
        }
    }
});