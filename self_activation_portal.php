<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>Self-Activation Portal</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->
<!-- ==== Self Activation Portal Section Start === -->
<div class="self_active_wrap">
    <section class="self_activation_section">
        <div class="container">
            <div class="row">
                <div class="self_activation_section_area">
                    <form>
                        <div class="form_step1">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                                <div class="header_content">
                                    <h2>Welcome to the Self-Activation Portal</h2>
                                    <p>Using this portal allows you to activate your service .</p>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-7 col-sm-12 col-12 mx-auto">
                                <div class="box_area">
                                    <div class="row">
                                        <div class="form_group_area">
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label for="mobile_account_number">Mobile account number</label>
                                                    <input type="text" class="form-control" placeholder="Enter your account number" />
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label for="zip_code">ZIP code</label>
                                                    <input type="text" class="form-control" placeholder="Enter ZIP code" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="button">
                                                <a href="javascript:;" class="next_step_2 btn btn-primary-1">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_step2">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                                <div class="header_content">
                                    <h2>Lines ready for activation</h2>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-7 col-sm-12 col-12 mx-auto">
                                <div class="box_area">
                                    <div class="row">
                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="box_header">    
                                                <h3>Line 1</h3>
                                            </div>   
                                            <h5><span>Status : <span>Ready For Activation</h5>
                                            <h5><span>Phone Number : <span>Click activate to start your service</h5>
                                            <h5><span>Serial Number (IMEI/MEID) : </span>355070814769240</h5>
                                            <h5><span>SIM Card Number : </span>89014103273626984250</h5>
                                        </div>
                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="button">
                                                <a href="#" class="btn btn-primary-1" data-bs-toggle="modal" data-bs-target="#actived">Activate</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="footer_content">
        <ul>
            <li>
                <p>Do not cancel your current wireless service before we complete the transfer of your number.</p>
            </li>
            <li>
                <p>Please have your Mobile on hand prior to starting self-activation.</p>
            </li>
            <li>
                <p>You will also need your account number and zip code.</p>
            </li>
        </ul>
    </div>
</div>
<!-- ==== Self Activation portal Section End === -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>

