<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard/sidebar.php') ?>
        <div class="right_side_wrap top-space">
        <div class="heading_area el">
                <div class="left">
                <h2>My transactions</h2>
                </div>
                <div class="right">
                    <div class="filter">
                        <ul>
                            <li class="dropdown actions_dropdown">
                                <a class="dropdown-toggle" id="actions" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="true">
                                    <i class="fas fa-filter"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="actions" data-popper-placement="bottom-end">
                                    <form action="">
                                        <div class="row">
                                            <!-- <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label for="">
                                                        Categories
                                                    </label>
                                                    <select id="inputcategory" class="form-select">
                                                        <option selected="">.....</option>
                                                        <option>....</option>
                                                        <option>....</option>
                                                        
                                                    </select>
                                                </div>
                                            </div> -->
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="row">
                                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <label for="">
                                                            Created On
                                                        </label>
                                                    </div>
                                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                        <div class="form-group">
                                                            <div class="input-group" id="start_date_box">
                                                                <input type="text" class="form-control" name="start_date" id="start_date" placeholder="Enter start date">
                                                                <div class="icon">
                                                                    <i class="fi fi-rr-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                        <div class="form-group">
                                                            <div class="input-group" id="new_date_box">
                                                                <input type="text" class="form-control" name="start_date" id="new_date" placeholder="Enter end date">
                                                                <div class="icon">
                                                                    <i class="fi fi-rr-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <div class="custom-control-radio custom-radio">
                                                        <input class="form-check-input" type="radio" name="where" id="r-1" value="1" checked="">
                                                        <label class="form-check-label" for="r-1">
                                                        Active
                                                        </label>
                                                        <input class="form-check-input" type="radio" name="where" id="r-2" value="1">
                                                        <label class="form-check-label" for="r-2">
                                                        Expired
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="sbt float-start">
                                                    <button type="submit" class="btn btn-primary">Reset all</button>
                                                </div>
                                                <div class="sbt">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="transaction">
                <div class="table_row_area">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="">
                                    <i class="fal fa-sort pe-2"></i>Transaction id
                                    </th>
                                    <th width="">
                                    <i class="fal fa-sort pe-2"></i>Order id
                                    </th>
                                    <th width="">
                                    <i class="fal fa-sort pe-2"></i>Amount
                                    </th>
                                    <th width="">
                                    <i class="fal fa-sort pe-2"></i>Date
                                    </th>
                                    <th width="">
                                    <i class="fal fa-sort pe-2"></i>Status
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="deactivate">
                                            Failed
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                                <tr class="abc">
                                    <td>
                                        harok256140
                                    </td>
                                    <td>
                                        <a href="dashboard_my_order_detail.php">#56123</a>
                                    </td>
                                    <td>₹1200</td>
                                    <td>
                                        02-12-2021
                                    </td>
                                    <td>
                                        <div class="">
                                            Paid
                                        </div>
                                    </td>             
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>      
            </div>
        </div>
    </div>
</section>


<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>