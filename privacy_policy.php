<?php include('common/header.php') ?>



<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>Privacy Policy</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->


<section class="cms_pages_section privacy_policy_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="privacy_heading">
                    <h2>Nullam interdum lorem ipsum sit dor aliquam lobortis dolor </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum suscipit quam non eleifend ornare. Nullam porta volutpat 
                        facilisis. Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. Duis id consectetur mi. Vivamus aliquam lorem 
                        lacus, ac molestie sapien sagittis eget. Cras eget elementum ligula. Donec efficitur quam ut tortor pretium molestie.
                         Fusce velit purus, cursus at risus non, laoreet tempus diam. Fusce vitae pretium lacus, ac malesuada nisi. </p>

                    <h2>lorem ipsum sit dor aliquam lobortis dolor. </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris. Nullam
                        interdum, purus et feugiat volutpat,
                        nunc tellus hendrerit ectus, vitae malesuada felis ex tempus diam. Etiam tristique euismod ante,
                        vel ornare urna. Suspendisse augue
                        purus, hendrerit vel lacus at, aliquam eleifend lacus. Nam quis nulla sollicitudin enim
                        scelerisque molestie. Suspendisse potenti.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor feugiat
                        volutpat, nunc tellus hendrerit ectus,
                        vitae malesuada felis ex tempus diam. Etiam tristique euismod
                        ante, vel ornare urna. Suspendisse augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                        Nam quis nulla sollicitudin enim
                        scelerisque molestie. Suspendisse potenti.</p>

                    <h2>lorem ipsum sit dor aliquam lobortis dolor.</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris. Nullam
                        interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex tempus diam. Etiam
                        tristique euismod
                        ante, vel ornare urna. Suspendisse augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                        Nam quis nulla
                        sollicitudin enim scelerisque molestie. Suspendisse potenti.Lorem ipsum dolor sit amet,
                        consectetur adipiscing
                        elit. Aliquam lobortis dolor feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada
                        felis ex tempus diam. Etiam
                        tristique euismod
                        ante, vel ornare urna. Suspendisse augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                        Nam quis nulla sollicitudin
                        enim scelerisque molestie. Suspendisse potenti. </p>

                    <h2>lorem ipsum sit dor aliquam lobortis dolor </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris. Nullam
                        interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus,
                        vitae malesuada felis ex tempus diam. Etiam tristique euismod ante, vel ornare urna.
                        Suspendisse augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex tempus diam. Etiam
                        tristique euismod ante,
                        vel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris.
                        Nullam interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex
                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse augue purus, hendrerit
                        vel lacus at, aliquam eleifend lacus.  </p>


                    <h2>Aliquam lobortis dolor</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris. Nullam
                        interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex tempus diam. Etiam
                        tristique euismod ante,
                        vel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris.
                        Nullam interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex
                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse augue purus, hendrerit
                        vel lacus at, aliquam eleifend lacus.
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex tempus diam. Etiam
                        tristique euismod ante,
                        vel Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis dolor mauris.
                        Nullam interdum, purus
                        et feugiat volutpat, nunc tellus hendrerit ectus, vitae malesuada felis ex
                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse augue purus, hendrerit
                        vel lacus at, aliquam eleifend lacus.  </p>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== Header === -->
<?php include('common/footer.php') ?>