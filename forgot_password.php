<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Reset Password Section Start === -->
<section class="login_section top-space">
<div class="container">
    <div class="row">
        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-7 col-sm-12 col-12 mx-auto">
            <div class="login_section_area">
                <div class="header_area">
                    <h3>Forgot password</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                </div>
                <div class="box_area forgot_password_area el">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="new_password">Email</label>
                                    <input type="email" class="form-control" placeholder="Enter your email address" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="login_button">
                                    <a href="#javascript" class="btn btn-primary-1">Send link</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- ==== Reset Password Section End === -->

<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?>

