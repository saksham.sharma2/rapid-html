<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="banner_section top-space" style="background-image: url('images/banback.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="banner_content_side">
                    <div class="left">
                        <h1>
                            Stay Connected <br><span>with Rapid</span>
                            </h1>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                        </p>
                        <div class="get">
                            <a href="coverage1.php" class="btn btn-primary-1">Get started<i class="far fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="right">
                        <div class="img_area">
                            <img src="images/group69.png" alt="..." />
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                    About us 
                    </h2>
                    <p>
                        Rapid Datacom is the US Veteran’s wireless provider. Our service is based on dependable, nationwide 4G and 5G LTE networks. We are committed to providing veterans with this dependable wireless service and exceptional support, while supporting other veterans through contributions to organizations such as Platoon 22 and Heroic Reins.
                    </p>
                </div>
                <div class="about_inner">
                    <div class="row">
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/Radio-Tower.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                     Nationwide <br>Coverage
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/LowPrice.png" alt=".." />
                                </div>
                                <div class="name">
                                    <p>
                                    No extra <br>overhead
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/directions1.png" alt=".." />
                                </div>
                                <div class="name">
                                    <p>
                                    flexible plan<br> option
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/connectivity1.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                    5G Coverage <br>lorem 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="newcustomer_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                    New customer offers
                    </h2>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum.
                    </p>
                </div>
                <div class="plans_inner">
                    <div class="plan_sliders">
                        <div class="owl-carousel owl-theme" id="plans">
                            <div class="item">
                                <div class="plans_ment">
                                    <div class="plan_gb">
                                        1GB
                                    </div>
                                    <div class="desp">
                                        <p>
                                            Unlimited text and talk
                                        </p>
                                    </div>
                                    <div class="pice">
                                        <h2>
                                            $24
                                        </h2>
                                        <p>
                                            Per line
                                        </p>
                                    </div>
                                    <div class="benfit">
                                        <ul>
                                            <li>
                                                <p>
                                                Lorem ipsum dolor sit amet.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit. 
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit Amet, dor dolor. 
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1"  data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="plans_ment">
                                    <div class="plan_gb">
                                        3GB
                                    </div>
                                    <div class="desp">
                                        <p>
                                            Unlimited text and talk
                                        </p>
                                    </div>
                                    <div class="pice">
                                        <h2>
                                            $34
                                        </h2>
                                        <p>
                                            Per line
                                        </p>
                                    </div>
                                    <div class="benfit">
                                        <ul>
                                            <li>
                                                <p>
                                                Lorem ipsum dolor sit amet.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit. 
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit Amet, dor dolor. 
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1"  data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="plans_ment">
                                    <div class="plan_gb">
                                        5GB
                                    </div>
                                    <div class="desp">
                                        <p>
                                            Unlimited text and talk
                                        </p>
                                    </div>
                                    <div class="pice">
                                        <h2>
                                            $44
                                        </h2>
                                        <p>
                                            Per line
                                        </p>
                                    </div>
                                    <div class="benfit">
                                        <ul>
                                            <li>
                                                <p>
                                                Lorem ipsum dolor sit amet.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit. 
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit Amet, dor dolor. 
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1"  data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="plans_ment">
                                    <div class="plan_gb">
                                        12GB
                                    </div>
                                    <div class="desp">
                                        <p>
                                            Unlimited text and talk
                                        </p>
                                    </div>
                                    <div class="pice">
                                        <h2>
                                            $54
                                        </h2>
                                        <p>
                                            Per line
                                        </p>
                                    </div>
                                    <div class="benfit">
                                        <ul>
                                            <li>
                                                <p>
                                                Lorem ipsum dolor sit amet.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit. 
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit Amet, dor dolor. 
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1"  data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="plans_ment">
                                    <div class="plan_gb">
                                        1GB
                                    </div>
                                    <div class="desp">
                                        <p>
                                            Unlimited text and talk
                                        </p>
                                    </div>
                                    <div class="pice">
                                        <h2>
                                            $24
                                        </h2>
                                        <p>
                                            Per line
                                        </p>
                                    </div>
                                    <div class="benfit">
                                        <ul>
                                            <li>
                                                <p>
                                                Lorem ipsum dolor sit amet.
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit. 
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    Lorem ipsum dolor sit Amet, dor dolor. 
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1"  data-bs-toggle="modal" data-bs-target="#plan">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sell-all float-end">
                            <a href="plan.php" class="btn btn-primary-1">See all plans</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how_work_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        How it works
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                    </p>
                </div>
                <div class="work_inner">
                    <div class="row">
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/Radio-Tower.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                    Check coverge in your area
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/Touchscreen.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                        Use your existing phone.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/Bulleted List.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                        Pick a plan , any plan
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <img src="images/Ok.png" alt="..." />
                                </div>
                                <div class="name">
                                    <p>
                                        Place your order & activate your plan
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <section class="featured_device_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        Featured Devices
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                    </p>
                </div>
                <div class="featured_device">
                    <div class="row">
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <a href="#">
                                        <img src="images/Rectangle 25.png" alt="..." />
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="#">
                                        <p>
                                            iPhone 12
                                        </p>
                                    </a>
                                </div>
                                <div class="price">
                                    <div class="left">
                                       <p>$25 <span>/ month</span></p>
                                       <span>
                                        Total price: $240
                                       </span>
                                    </div>
                                    <div class="right">
                                        <a href="#">
                                            <img src="images/shopping.png" alt="..." />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <a href="#">
                                        <img src="images/Rectangle 28.png" alt="..." />
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="#">
                                        <p>
                                            Samsung A52
                                        </p>
                                    </a>
                                </div>
                                <div class="price">
                                    <div class="left">
                                       <p>$20 <span>/ month</span></p>
                                       <span>
                                        Total price: $240
                                       </span>
                                    </div>
                                    <div class="right">
                                        <a href="#">
                                            <img src="images/shopping.png" alt="..." />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <a href="#">
                                        <img src="images/Rectangle29.png" alt="..." />
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="#">
                                        <p>
                                            Samsung A52
                                        </p>
                                    </a>
                                </div>
                                <div class="price">
                                    <div class="left">
                                       <p>$20 <span>/ month</span></p>
                                       <span>
                                        Total price: $240
                                       </span>
                                    </div>
                                    <div class="right">
                                        <a href="#">
                                            <img src="images/shopping.png" alt="..." />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-md-4 col-sm-6 col-12">
                            <div class="network match_height_col">
                                <div class="img_area">
                                    <a href="#">
                                        <img src="images/Rectangle97.png" alt="..." />
                                    </a>
                                </div>
                                <div class="name">
                                    <a href="#">
                                        <p>
                                            OnePlus 9R
                                        </p>
                                    </a>
                                </div>
                                <div class="price">
                                    <div class="left">
                                       <p>$20 <span>/ month</span></p>
                                       <span>
                                        Total price: $240
                                       </span>
                                    </div>
                                    <div class="right">
                                        <a href="#">
                                            <img src="images/shopping.png" alt="..." />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                            <div class="see float-end">
                                <a href="#" class="btn btn-primary-1">See all</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section class="nation_wide_secion">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-sm-12 col-12 p-0">
                <div class="nation_inne">
                    <div class="nation_inne_area">
                        <div class="left">
                            <img src="images/map.png" alt="..." />
                        </div>
                        <div class="right">
                            <img src="images/12.png" alt="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="locat_main">
        <div class="loact">
            <div class="heading_set">
                <h2>
                    Nationwide Coverage
                </h2>
                <p>
                    Check the coverage in your area.
                </p>
                <div class="zip-code">
                    <form action="">
                        <div class="form-group">
                            <input type="number" class="form-control" id="zipcode" placeholder="Zip code">
                            <a href="coverage1.php" class="btn btn-primary-1">Let's go</a>
                            <div class="icon">
                                <i class="fal fa-map-marker-alt"></i>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        What people say about us 
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                    </p>
                </div>
                <div class="testimonial_slider">
                    <div class="owl-carousel owl-theme" id="testimonial">
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/testminial1.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/testminial3.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/testminial2.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slider_area">
                                <div class="rating">
                                    <ul>
                                        <li>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fal fa-star"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content match_height_col">
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam convallis libero elit, at rhoncus neque dignissim ac. Morbi suscipit nisl magna. Aliquam hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi suscipit nisl magna. Aliquam hendrerit.
                                    </p>
                                </div>
                                <div class="user_area">
                                    <div class="img_box">
                                        <img src="images/testminial2.png" alt="..." />
                                    </div>
                                    <div class="name">
                                        <h5>
                                            Jane Cooper
                                        </h5>
                                        <p>
                                            lorem ipsum
                                        </p>
                                        <div class="quto">
                                            <img src="images/quote-sign.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== footer === -->
<?php include('common/footer.php') ?>