<?php include('common/header.php') ?>
<section class="checkout_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="checkout-inner">
                    <div class="row">
                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <div class="heading_area">
                                <h1>Checkout</h1>
                            </div>
                            <div class="modal_area">
                                <div class="plan_gb">1GB</div>
                                <div class="cross_area">
                                    <a href="#" class="btn">
                                        <i class="far fa-times"></i>
                                    </a>
                                </div>
                                <div class="items_detail_area">
                                    <div class="item">
                                        <div class="plans_ment">
                                            <div class="desp">
                                                <p>
                                                    Unlimited text and talk
                                                </p>
                                            </div>
                                            <div class="price">
                                                <h2>
                                                    $24 <sub>/Per Month</sub>
                                                </h2>
                                            </div>

                                            <div class="detail_area">
                                                <p>Unlimited talk and plan data usage in USA, Canada, Mexico, Puerto
                                                    Rico, and US Virgin Islands.
                                                    Unlimited text to Canada, Mexico, and more than 190 countries. 1GB
                                                    data, 4G & 5G LTE, Wi-Fi
                                                    calling.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contact_info">
                                        <p>Number of line : 1 line</p>
                                        <p>Phone number : +1 456 3256 1564</p>
                                        <p>IMEI : 355070814769240</p>
                                    </div>
                                </div>
                            </div>
                            <div class="detail_area">
                                <div class="accordion" id="accordionExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="headingThree">
                                            <button class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                aria-expanded="false" aria-controls="collapseThree">
                                                Payment options
                                            </button>
                                        </h2>
                                        <div id="collapseThree" class="accordion-collapse collapse"
                                            aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="radio_area">
                                                            <div class="form-check">
                                                                <input class="form-check-input credit" type="radio"
                                                                    name="flexRadioDefault"  id="flexRadioDefault1" checked>
                                                                <label class="form-check-label credit" for="flexRadioDefault1">
                                                                    Credit card
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="credit_detail">
                                                            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                                                                <div class="form-group">
                                                                    <label>Name</label>
                                                                    <input type="text" class="form-control" name=""
                                                                        placeholder="Enter your name">
                                                                </div>
                                                            </div>
                                                            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                                                                <div class="form-group">
                                                                    <label>Card number</label>
                                                                    <div class="form_icon">
                                                                        <input type="number" class="form-control" name=""
                                                                            placeholder="Enter your card number">
                                                                        <div class="icon_area">
                                                                            <i class="far fa-lock"></i>
                                                                        </div>
                                                                    </div>
                                                                    <p>Pay with your MasterCard, Visa, Discover or American Express
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 ">
                                                                <div class="form-group">
                                                                    <label>Expiration date</label>
                                                                    <input type="text" class="form-control" name=""
                                                                        placeholder="00/00">
                                                                </div>
                                                            </div>
                                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 ">
                                                                <div class="form-group">
                                                                    <label>Security code</label>
                                                                    <div class="form_icon">
                                                                        <input type="text" class="form-control" name=""
                                                                            placeholder="***">
                                                                        <div class="icon_area">
                                                                            <i class="far fa-lock"></i>
                                                                        </div>
                                                                    </div>
                                                                    <p>Your information is secure. </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="radio_area el">
                                                                <div class="form-check">
                                                                    <input class="form-check-input paypal" type="radio"
                                                                        name="flexRadioDefault"   id="flexRadioDefault2">
                                                                    <label class="form-check-label paypal"  for="flexRadioDefault2">
                                                                        PayPal
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="paypals d-none">
                                                                <a href="#" class="btn btn-primary-1">Paypal</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
                            <div class="detail_area">
                                <h1>Price details</h1>
                                <div class="price_detail_area">
                                    <div class="heading_area">

                                        <div class="price_area">
                                            <ul>
                                                <li>
                                                    <div class="left">
                                                        <p>
                                                            Price
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <p>
                                                            $24.00
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="left">
                                                        <p>
                                                            Activation fee
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <p>
                                                            $25.00
                                                        </p>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="total_area">
                                            <div class="left">
                                                <p>
                                                    Total
                                                </p>
                                            </div>
                                            <div class="right">
                                                <p>
                                                    $49.00
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="view">
                                        <a href="#" class="btn btn-primary-1">Pay $49.00</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Header === -->
<?php include('common/footer.php') ?>