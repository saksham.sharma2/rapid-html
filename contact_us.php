<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame Section Start ==== -->
<section class="breadcame_section" style="background-image: url('images/breadcame.png');">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area text-center">
                <h2>CONTACT US</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame Section End ==== -->



<section class="contact-wrap">
    <div class="container">
        <div class="cont-bck-main">
            <div class="cont-bck">
                <div class="row">
                    <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 p-0">
                    <div class="query-side">
                            <div class="query-title-head-line">
                                <h3>Get in touch</h3>
                                <p>Fill up the form and our team will get back to you within 24 hours!</p>
                            </div>
                            <div class="eleat-contact-query">
                                <ul>
                                    <li class="map">
                                        <a href="javascript:;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. </a>
                                    </li>
                                    <li class="phone">
                                        <a href="telto:1236478636">+123 647 8636</a>
                                    </li>
                                    <li class="email">
                                        <a href="mailto:info@samplemail.com">info@samplemail.com</a>
                                    </li>
                                   
                                </ul>
                            </div>
                            <div class="social-share">
                                <ul>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="contact_time">
                                <div class="contact_time_wrap">
                                    <p>
                                        Monday - Friday: 9am - 5pm
                                    </p>
                                    <p>
                                        Saturday - Sunday: Closed
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 p-0 ">
                        <div class="query-form">
                            <form class="row">
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label for="">  First name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter your first name">
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-group">
                                        <label for="">Last name</label>
                                        <input type="text" class="form-control" id="lname" placeholder="Enter your last name">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" class="form-control" id="eamil" placeholder=" Enter your email address">
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Phone number</label>
                                        <input type="number" class="form-control" id="cell" placeholder=" Enter your phone number">
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">City</label>
                                        <input type="number" class="form-control" id="city" placeholder=" Enter city">
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Zip code</label>
                                        <input type="number" class="form-control" id="zip" placeholder=" Enter zip code">
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                    <label for="">Subject</label>
                                        <select id="inputcategory" class="form-select">
                                                <option selected="">Subject</option>
                                                <option>..</option>
                                        </select>
                                        </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="" class="form-control" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="subiter">
                                        <button type="submit" class="btn btn-primary-1">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mapes">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13004069.896900944!2d-104.65611544442767!3d37.27565371492453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2sin!4v1649395167974!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="locat">
                            <div class="icon">
                                <i class="fal fa-map-pin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- ==== Footer ==== -->
<?php include('common/footer.php') ?> 